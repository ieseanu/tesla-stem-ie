package com.example.sandbox;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.sandbox.MainActivity.RC_DATA;

public class SecondActivity extends AppCompatActivity {

    public final static String KEY_RETURN = "key.return";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent i = getIntent();
        String value = ((Intent) i).getStringExtra(MainActivity.KEY_DATA);
        TextView textView = findViewById(R.id.second_screen);
        textView.setText(value);

        textView.setOnClickListener(new View.OnClickListener()){
            @Override
            public void onClick(View v){
                Intent i = new Intent(SecondActivity.this, MainActivity.class);
                i.putExtra(KEY_RETURN, "Value from SecondActivity");
                setResult(RESULT_OK, i);
                finish();
            }

            @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
                 if (resultCode == RESULT_OK) {
                    if (requestCode == RC_DATA) {
                        String text = data.getStringExtra(SecondActivity.KEY_RETURN);
                         Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
                    }
                 }
            }

        }
    }


}