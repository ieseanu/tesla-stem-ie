package com.example.idraw;

import java.util.ArrayList;
import java.util.List;

public class DrawingRegistry {
    private static DrawingRegistry registry;
    private List<Drawing> drawings;

    public static DrawingRegistry getInstance() {
        if (registry == null) {
            registry = new DrawingRegistry();
        }
        return registry;
    }

    private DrawingRegistry() {
        drawings = new ArrayList<>();
    }

    public void addDrawing(String name) {
        Drawing drawing = new Drawing();
        drawing.setName(name);
        drawings.add(drawing);
    }

    public List<Drawing> getDrawings() {
        return drawings;
    }
}
