package com.example.idraw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DrawingActivity extends AppCompatActivity {

    private static int drawingNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing);

        final DrawingView view = findViewById(R.id.drawing_view);
        Intent i = getIntent();
        int drawingIndex = i.getIntExtra("drawing", -1);


        if(drawingIndex == -1) {
            DrawingRegistry.getInstance().addDrawing("drawing_" + drawingNumber);
            drawingNumber++;
            drawingIndex = DrawingRegistry.getInstance().getDrawings().size() - 1;
        }
        Drawing drawing = DrawingRegistry.getInstance().getDrawings().get(drawingIndex);
        view.setDrawing(drawing);

        TextView save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

}
