package com.example.idraw;

import android.graphics.Paint;
import android.graphics.Path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Drawing implements Serializable {
    private UUID id;
    private String name;
    private ArrayList<Path> pathList = new ArrayList<>();
    private ArrayList<Paint> paintList = new ArrayList<>();

    public Drawing() {
        id = UUID.randomUUID();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Path> getPathList() {
        return pathList;
    }

    public List<Paint> getPaintList() {
        return paintList;
    }
}
