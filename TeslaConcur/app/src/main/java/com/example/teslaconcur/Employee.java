package com.example.teslaconcur;

import java.util.UUID;

public class Employee {
    private UUID id;
    private String firstName;
    private String lastName;

    public Employee() {
        id = UUID.randomUUID();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}

