package com.example.teslaconcur;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.teslaconcur.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Employee> list;

    public RecyclerViewAdapter(List<Employee> list) {
        this.list = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ViewHolder(RelativeLayout view) {
            super(view);
            name = view.findViewById(R.id.name);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.employee_item, parent, false);
        ViewHolder vh = new ViewHolder(layout);
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent i = new Intent(context, EmployeeActivity.class);
                context.startActivity(i);
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Employee employee = list.get(position);
        String firstName = employee.getFirstName();
        String lastName = employee.getLastName();
        holder.name.setText(firstName + " " + lastName);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

