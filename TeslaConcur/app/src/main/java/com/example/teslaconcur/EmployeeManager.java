package com.example.teslaconcur;

import java.util.ArrayList;
import java.util.List;

public class EmployeeManager {
    private static EmployeeManager manager;
    private List<Employee> employees;

    public static EmployeeManager getInstance() {
        if (manager == null) {
            manager = new EmployeeManager();
        }
        return manager;
    }

    private EmployeeManager() {
        employees = new ArrayList<>();
    }

    public void add(String firstName, String lastName) {
        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employees.add(employee);
    }

    public List<Employee> getEmployees() {
        return employees;
    }
}
